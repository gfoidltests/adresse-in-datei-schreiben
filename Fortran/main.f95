program adresseDemo
	use types
	implicit none
	type(adresse)			:: addr
	integer, parameter		:: file = 11, recl = sizeof(addr)
	integer					:: iostat
	
	! open(file, file='adressen.dat', iostat=iostat, status='replace', action='write', form='unformatted')
	open(file, file='adressen.dat', iostat=iostat, status='replace', action='write', form='unformatted', access='direct', recl=recl)
	if (iostat == 0) then
		call schreibe_adresse(file, 'Erika Mustermann', 12345)
		call schreibe_adresse(file, 'Hans Müller', 54321)
		call schreibe_adresse(file, 'Secret Services', 700)
		call schreibe_adresse(file, 'Peter Mustermann', 12345)
		call schreibe_adresse(file, 'Wikibook Nutzer', 99999)
		
		close(file)
	else
		print *, 'Fehler beim oeffnen der Datei'
	end if
	
	open(file, file='adressen.dat', iostat=iostat, status='old', action='read', form='unformatted', access='direct', recl=recl)
	if (iostat == 0) then
		addr = lese_adresse(file, 4)
		write(*,*) addr
	else
		print *, 'Fehler beim oeffnen der Datei'
	end if

	print *, 'bye'
end program adresseDemo