module types
	implicit none
	integer, parameter			:: strLen = 25
	private mache_adresse
	
	type adresse
		sequence
		character(len=strLen)	:: name
		integer					:: plz
	end type adresse
	
	contains
		function mache_adresse(name, plz)
			type(adresse)						:: mache_adresse
			character(len=*), intent(in)		:: name
			integer, intent(in)					:: plz
			
			mache_adresse%name = name
			mache_adresse%plz = plz
		end function mache_adresse
		
		subroutine schreibe_adresse(file, name, plz)
			integer, intent(in)					:: file, plz
			character(len=*), intent(in)		:: name
			type(adresse)						:: addr
			integer, save						:: counter = 0
			
			counter = counter + 1			
			addr = mache_adresse(name, plz)
			
			! write(file, '(a,i5)') addr%name, addr%plz
			! write(file) addr			! kein Format -> sonst Fehler
			
			! direct -> sonst passt es beim Lesen nicht so ganz zusammen ;-)
			write(file, rec=counter) addr
		end subroutine schreibe_adresse
		
		function lese_adresse(file, position)
			type(adresse)						:: lese_adresse
			integer, intent(in)					:: file, position
			
			read(file, rec=position) lese_adresse
		end function lese_adresse
end module types