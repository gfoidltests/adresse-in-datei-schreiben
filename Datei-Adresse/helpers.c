#include <stdio.h>
#include "types.h"

//void mache_adresse(adresse *a, const char *name, const int plz)
adresse mache_adresse(const char *name, const int plz)
{
	adresse a;

	sprintf(a.name, "%.99s", name);
	a.plz = plz;

	return a;
}

void schreibe_adresse(FILE *datei, const char *name, const int plz)
{
	adresse addr;
	//mache_adresse(&addr, name, plz);
	addr = mache_adresse(name, plz);
	fwrite(&addr, sizeof(adresse), 1, datei);
}

adresse lese_adresse(FILE *datei, int position)
{
	adresse addr;

	fseek(datei, (position - 1) * sizeof(adresse), SEEK_SET);
	fread(&addr, sizeof(adresse), 1, datei);

	return addr;
}