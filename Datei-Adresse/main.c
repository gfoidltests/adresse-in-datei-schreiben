#include <stdio.h>
#include <string.h>
#include "types.h"
#include "helpers.h"

int main(void)
{
	FILE *datei;
	adresse addr;

	datei = fopen("adressdaten.dat", "wb");
	if (datei != NULL)
	{
		schreibe_adresse(datei, "Erika Mustermann", 12345);
		schreibe_adresse(datei, "Hans M�ller", 54321);
		schreibe_adresse(datei, "Secret Services", 700);
		schreibe_adresse(datei, "Peter Mustermann", 12345);
		schreibe_adresse(datei, "Wikibook Nutzer", 99999);

		fclose(datei);
	}

	datei = fopen("adressdaten.dat", "rb");
	if (datei != NULL)
	{
		addr = lese_adresse(datei, 4);
		printf("Name: %s (%d)\n", addr.name, addr.plz);
		fclose(datei);
	}

	return 0;
}